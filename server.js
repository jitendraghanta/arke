var express = require('express');
var bodyParser = require('body-parser');
var pg = require('pg');
var app = express();
app.set('port', process.env.PORT || 5000);

var router = express.Router();
app.use(router);
app.use(express.static(__dirname+'/production'));
app.use(express.static(__dirname+'/production/js'));
app.use(express.static(__dirname+'/production/css'));
app.use(express.static(__dirname+'/production/images'));
app.use(express.static(__dirname+'/production/vendors'));
app.use(express.static(__dirname+'/production/views'));
app.use(bodyParser.json());
app.use('/',router);

app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});


 