var app = angular.module("ttApp",["ui.router"]);


app.directive('leftNav', function() {
  return {
    restrict: 'E',
    templateUrl: '../views/leftNav.html'
  };
}).directive('topNav', function() {
  return {
    restrict: 'E',
    templateUrl: '../views/topNav.html',
    controller:['$scope', function MyTabsController($scope) {
    	$scope.notifications = [{profilepic:''}];
    }]
  };
})

app.config(['$stateProvider', '$urlRouterProvider','$httpProvider' ,function($stateProvider, $urlRouterProvider,$httpProvider){
  $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    $urlRouterProvider.otherwise("/login");
	  $stateProvider
		.state('login', {
			url: '/login',
			templateUrl: '../views/login.html',
      controller:['$scope','$http',function($scope,$http){
          alert('hai');
          $http.get('http://10.10.44.117:8088/api/TimeTrickObject/Get').then(function(result){
            alert(result);
          })
      }]
		})   
    .state('main', {
      url: '/main',
      templateUrl: '../master.html',
    })
  
   
}])